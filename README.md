# ug

ug is :

- like a caveman
- partly ugly
- **u**nit test **g**enerator

Give it a file path to a JS file that exports a react-redux connected component and it will generate a unit test next to the file.

The generated unit test will mock out all dependencies and spit out all boilerplate for this kind of test.

It won't cover any logic that's more than simply calling a selector for each prop in `mapStateToProps` (and mapping an action creator directly in `mapDispatchToProps`).

It can handle use of `withRouter`.

## How to use

Clone, build and install:

        git clone https://bitbucket.org/krowten/ug.git
        cd ug
        npm i && npm run build
        npm i -g .

Run:

        ug <full file path of connect component to test>

---

If you prefer not to install it:

    git clone https://bitbucket.org/krowten/ug.git
    cd ug
    npm i && npm run build
    node index <full file path of connect component to test>

# Upgrade

In the ug dir:

    git pull
    npm i && npm run build
    npm i -g .
