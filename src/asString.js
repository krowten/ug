export const valueAsString = value =>
  typeof value === "string" && value.startsWith("_identifier_")
    ? value.replace("_identifier_", "")
    : JSON.stringify(value);

export default (obj, indent = 6) => {
  let output = "{ \n";
  Object.keys(obj).forEach(i => {
    output += "        ";
    if (obj[i] === i) {
      output += `${i}, \n`;
    } else {
      const value = valueAsString(obj[i]);
      output += `${i}: ${value}, \n`;
    }
  });
  output += new Array(indent).join(" ");
  output += "}";
  return output;
};
