import { parse } from "meriyah";
import fs from "fs";

import asString, { valueAsString } from "./asString";

const sourceFilePath = process.argv.slice(2)[0];

if (!sourceFilePath) {
  console.info("Usage: ug <full file path of connect component to test>");
  process.exit();
}
const source = fs.readFileSync(sourceFilePath, "UTF-8");
const tree = parse(source, { module: true });
fs.writeFileSync(
  `${__dirname}/../dump.json`,
  JSON.stringify(tree, null, 4),
  "UTF-8"
);

const pathImports = {};
const pathMocks = {};
let mockWithRouter;
let selectorsToTest = [];
const selectorToTestArgs = {};

let componentName;

let mapStateToPropsObject;
let mapStateToPropsArgs;
let mapDispatchToPropsUsesDispatch;
let mapDispatchToPropsUsesProps;
let mapDispatchToPropsObject;
let mergeProps;

tree.body.forEach(node => {
  if (node.type == "ImportDeclaration") {
    node.specifiers.forEach(importSpecifier => {
      if (!pathImports[node.source.value]) {
        pathImports[node.source.value] = {
          defaultImports: [],
          namedImports: []
        };
      }

      if (importSpecifier.type == "ImportDefaultSpecifier") {
        pathImports[node.source.value].defaultImports.push(
          importSpecifier.local.name
        );

        let mock = `'${importSpecifier.local.name}'`;
        if (node.source.value.includes("selectors")) {
          mock = `jest.fn(() => '${importSpecifier.local.name} result')`;
          selectorsToTest.push(importSpecifier.local.name);
        }

        if (node.source.value.includes("actions")) {
          mock = `jest.fn(() => '${importSpecifier.local.name} action')`;
        }

        if (!pathMocks[node.source.value]) pathMocks[node.source.value] = [];
        pathMocks[node.source.value].push(
          ` __esModule: true, default: ${mock} `
        );
      }

      if (importSpecifier.type == "ImportSpecifier") {
        if (importSpecifier.local.name !== importSpecifier.imported.name) {
          pathImports[node.source.value].namedImports.push(
            `${importSpecifier.imported.name} as ${importSpecifier.local.name}`
          );
        } else {
          pathImports[node.source.value].namedImports.push(
            importSpecifier.imported.name
          );
        }

        let mock = `'${importSpecifier.imported.name}'`;

        if (node.source.value == "react-redux") {
          mock = `jest.fn(() => jest.fn(component => \`connected \${component}\`))`;
        }

        if (node.source.value == "react-router-dom") {
          mock = `jest.fn(component => \`\${component} with router\`)`;
        }

        if (node.source.value.includes("selectors")) {
          mock = `jest.fn(() => '${importSpecifier.imported.name} result')`;
          selectorsToTest.push(importSpecifier.imported.name);
        }

        if (node.source.value.includes("actions")) {
          mock = `jest.fn(() => '${importSpecifier.imported.name} action')`;
        }

        if (!pathMocks[node.source.value]) {
          pathMocks[node.source.value] = [];
        }
        pathMocks[node.source.value].push(
          ` ${importSpecifier.imported.name}: ${mock} `
        );
      }
    });
  }

  if (node.type == "ExportDefaultDeclaration") {
    let connectFunction;

    if (node.declaration.callee.name == "withRouter") {
      connectFunction = node.declaration.arguments[0];
      mockWithRouter = true;
    } else {
      // export of connect directly
      connectFunction = node.declaration;
    }
    componentName = connectFunction.arguments[0].name;

    if (connectFunction.callee.callee.name == "connect") {
      // mapStateToProps
      const connectArg1 = connectFunction.callee.arguments[0];
      let mapStateToProps = connectArg1;

      if (connectArg1?.type === "Identifier") {
        mapStateToProps = tree.body.find(
          n =>
            n.type == "VariableDeclaration" &&
            n.declarations[0].id.name === connectArg1.name
        ).declarations[0].init;
      }

      if (mapStateToProps) {
        mapStateToPropsObject = {};

        if (mapStateToProps.type === "ArrowFunctionExpression") {
          let mapStateToPropsProperties;
          if (mapStateToProps.body.properties) {
            mapStateToPropsProperties = mapStateToProps.body.properties;
          } else if (mapStateToProps.body.type === "BlockStatement") {
            mapStateToPropsProperties = mapStateToProps.body.body.find(
              a => a.type === "ReturnStatement"
            ).argument.properties;
          }

          mapStateToPropsProperties.forEach(property => {
            let value = "?";
            if (property.value?.type == "CallExpression") {
              value = `${property.value.callee.name}`;
              if (selectorsToTest.includes(property.value.callee.name)) {
                value = `${property.value.callee.name} result`;
                selectorToTestArgs[
                  property.value.callee.name
                ] = property.value.arguments.map(arg => {
                  if (arg.type == "Identifier") {
                    return `_identifier_${arg.name}`;
                  }
                  if (arg.type == "Literal") {
                    return arg.value;
                  }
                  if (arg.type == "ObjectExpression") {
                    return arg.properties.reduce((acc, p) => {
                      acc[p.key.name] = p.value.value || p.value.name;
                      return acc;
                    }, {});
                  }
                  return "??";
                });
              }
            }
            if (property.value?.type == "Literal") {
              value = property.value.value;
            }
            if (property.value?.type == "Identifier") {
              value = `_identifier_${property.value.name}`;
            }
            mapStateToPropsObject[property.key?.name] = value;
          });

          mapStateToPropsArgs = mapStateToProps.params?.map(arg => {
            if (arg.type == "Identifier") {
              return `_identifier_${arg.name}`;
            }
            if (arg.type == "ObjectPattern") {
              return arg.properties.reduce((acc, p) => {
                acc[p.key.name] = p.value.value || p.value.name;
                return acc;
              }, {});
            }
          });
        }
      }

      // mapDispatchToProps
      const connectArg2 = connectFunction.callee.arguments[1];
      let mapDispatchToProps = connectArg2;

      if (connectArg2?.type === "Identifier") {
        mapDispatchToProps = tree.body.find(
          n =>
            n.type == "VariableDeclaration" &&
            n.declarations[0].id.name === connectArg2.name
        ).declarations[0].init;
      }

      if (mapDispatchToProps) {
        mapDispatchToPropsObject = {};

        if (mapDispatchToProps.type == "ArrowFunctionExpression") {
          if (mapDispatchToProps.params.length) {
            mapDispatchToPropsUsesDispatch = true;
          }
          if (mapDispatchToProps.params.length > 1) {
            mapDispatchToPropsUsesProps = true;
          }
          mapDispatchToProps = mapDispatchToProps.body;
        }

        if (mapDispatchToProps.type == "ObjectExpression") {
          mapDispatchToProps.properties.forEach(property => {
            let value = "?";

            if (property.value.type == "Identifier") {
              value = `_identifier_${property.value.name}`;
            }

            mapDispatchToPropsObject[property.key.name] = value;
          });
        }
      }

      // mergeProps
      const connectArg3 = connectFunction.callee.arguments[2];
      let mergeProps = connectArg3;
    }
  }
});

const imports = Object.keys(pathImports).map(
  path =>
    `import ${pathImports[path].defaultImports.join(", ")}${
      pathImports[path].defaultImports.length &&
      pathImports[path].namedImports.length
        ? ", "
        : ""
    }${
      pathImports[path].namedImports.length
        ? `{ ${pathImports[path].namedImports.join(", ")} }`
        : ""
    } from '${path}';`
);

const importMocks = Object.keys(pathMocks).map(
  path => `jest.mock('${path}', () => ({ ${pathMocks[path].join(", ")} }));`
);

const connectedComponentName = `Connected${componentName}`;
const connectedComponentPath = sourceFilePath
  .substring(sourceFilePath.lastIndexOf("src") + 4)
  .replace("/index.js", "")
  .replace(".js", "");

let output = `
${imports.join("\n")}

import ${connectedComponentName} from '${connectedComponentPath}';

${importMocks.join("\n\n")}

describe('${componentName} connected component', () => {

  test('returns connected component', () => {
    expect(${connectedComponentName}).toBe('connected ${componentName}${
  mockWithRouter ? " with router" : ""
}');
  });
`;

if (mapStateToPropsObject) {
  const argState = mapStateToPropsArgs[0];
  const argProps = mapStateToPropsArgs[1];

  const connectMapStateToPropsCall = `connectMapStateToProps(${
    argState ? "state" : "undefined"
  }${argProps ? ", props" : ""})`;

  output += `

  describe('maps state to props correctly', () => {
    const connectMapStateToProps = connect.mock.calls[0][0];
    ${argState ? "const state = {};" : ""}
    ${argProps && typeof argProps == "string" ? `const ${argProps} = {};` : ""}
    ${
      argProps && typeof argProps !== "string"
        ? `const props = ${asString(argProps)};\n`
        : ""
    }
    test('connectMapStateToProps returns correct map', () => {
      expect(${connectMapStateToPropsCall}).toEqual(${asString(
    mapStateToPropsObject
  )});
    });
    ${
      selectorsToTest.length == 0
        ? ""
        : `
    describe('selectors are called correctly', () => {
      ${connectMapStateToPropsCall};
      ${selectorsToTest
        .map(
          selector => `
        test('${selector}', () => {
          expect(${selector}).toHaveBeenCalledWith(${selectorToTestArgs[
            selector
          ]
            ?.map(a =>
              argProps?.hasOwnProperty(valueAsString(a))
                ? `props.${valueAsString(a)}`
                : valueAsString(a)
            )
            .join(", ")})
        });
      `
        )
        .join("\n")}
    });
    `
    }
  });
 `;
}

if (mapDispatchToPropsObject && !mapDispatchToPropsUsesDispatch) {
  output += `

  test('maps dispatch to props correctly', () => {
    const connectMapDispatchToProps = connect.mock.calls[0][1];

    expect(connectMapDispatchToProps).toEqual(${asString(
      mapDispatchToPropsObject
    )});
    });
 `;
}

if (mapDispatchToPropsObject && mapDispatchToPropsUsesDispatch) {
  output += `

  describe('maps dispatch to props correctly', () => {
    const connectMapDispatchToProps = connect.mock.calls[0][1];
    const dispatch = jest.fn();
    ${mapDispatchToPropsUsesProps ? "const props = {};" : ""}

    const mappedProps = connectMapDispatchToProps(dispatch${
      mapDispatchToPropsUsesProps ? ", props" : ""
    });

    ${Object.keys(mapDispatchToPropsObject)
      .map(
        f => `
      test('${f}', () => {
        mappedProps.${f}();
        expect(dispatch).toHaveBeenCalledWith()
      })`
      )
      .join("\n\n")}

  });
 `;
}

if (mergeProps) {
  output += `
  
  describe('merges props correctly', () => {
    const mergeProps = connect.mock.calls[0][2];
    const mappedStateProps = {};
    const mappedDispatchProps = {};
    const componentProps = {};

    const mergedProps = mergeProps(mappedStateProps, mappedDispatchProps, componentProps);

    test('needs tests', () => {
      expect(mergeProps).toBeUndefined();
    });
  });
  `;
} 

output += `

  test('connected wrapper called with actual component', () => {
      const connectComponent = connect.mock.results[0].value;
      expect(connectComponent).toHaveBeenCalledWith(${componentName});
  });

});

`;

const outputFilePath = sourceFilePath.replace(".js", ".spec.js");
fs.writeFileSync(outputFilePath, output, "UTF-8");
console.log(output, "\n  written to \n", outputFilePath);
